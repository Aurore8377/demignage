-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  Dim 08 déc. 2019 à 21:31
-- Version du serveur :  8.0.18
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projet`
--

-- --------------------------------------------------------

--
-- Structure de la table `enigmes`
--

DROP TABLE IF EXISTS `enigmes`;
CREATE TABLE IF NOT EXISTS `enigmes` (
  `numero` varchar(7) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `enigme` varchar(400) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `taille` varchar(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `reponse` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `explication` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `reponse2` varchar(40) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `enigmes`
--

INSERT INTO `enigmes` (`numero`, `enigme`, `image`, `taille`, `reponse`, `explication`, `reponse2`) VALUES
('enigme1', 'Quel monument se cache derrière cette image ?', 'images/enigme1.png', '100%', 'statue de la liberte', 'Il s\'agit d\'un tableau d\'Eugène De La Croix, la Liberté guidant le peuple, dans lequel le drapeau de la France est remplacé par celui de New York', 'la statue de la liberte'),
('enigme2', 'Trouve quel dieu égyptien se cache derrière cette image ! La bonne réponse te permettra de récupérer une clé', 'images/enigme2.png', '100%', 're', 'Dans la mythologie égyptienne, Rê (aussi appelé Râ) était le dieu du Soleil', 'ra'),
('enigme3', 'Le parchemin t\'indique le prochain monument, trouve le !', 'images/enigme3.png', '100%', 'opera de sydney', 'En résolvant l\'énigme à l\'aide de l\'alphabet codé, le monument trouvé est bien l\'Opéra de Sydney', 'l\'opera de sydney'),
('enigme4', 'Sur un fauteuil de l\'Opéra, César a chiffré le prochain monument, trouve le !', 'images/enigme4.png', '100%', 'christ redempteur de rio', 'Si on remplace chaque lettre par la lettre qui la précède dans l\'alphabet, on trouve le Christ Redempteur de Rio', 'le christ redempteur de rio'),
('enigme5', 'Le Christ te tend un rébus menant au prochain monument, trouve le !', 'images/enigme5.png', '100%', 'cite interdite de pekin', 'Le rébus donne Scie Thé 1 Terre 10 t 2 Pecs 1 (Cité Interdite de Pékin)', 'la cite interdite de pekin');

-- --------------------------------------------------------

--
-- Structure de la table `icones`
--

DROP TABLE IF EXISTS `icones`;
CREATE TABLE IF NOT EXISTS `icones` (
  `icone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `zoom` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `taille` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `couche` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(40) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `icones`
--

INSERT INTO `icones` (`icone`, `latitude`, `longitude`, `zoom`, `image`, `taille`, `couche`, `nom`) VALUES
('tourIcon', '48.858370', '2.294481', '17', 'images/tourEiffelIcon.png', '100', 'tourEiffel', 'la Tour Eiffel'),
('statueIcon', '40.689249', '-74.044500', '17', 'images/StatueLiberteIcon.png', '100', 'statueLiberte', 'la Statue de la Liberté'),
('pyramideIcon', '29.979235', '31.134202', '17', 'images/pyramideKheopsIcon.png', '100', 'pyramide', 'la Pyramide de Khéops'),
('operaIcon', '-33.858317', '151.214726', '17', 'images/operaSydneyIcon.png', '100', 'operaSydney', 'l\'Opéra de Sydney'),
('christIcon', '-22.952703', '-43.211299', '17', 'images/christRioIcon.png', '100', 'christ', 'le Christ Rédempteur'),
('citeIcon', '39.912407', '116.396721', '17', 'images/citeInterditeIcon.png', '100', 'cite', 'la Cité Interdite'),
('cleIcon', '40.689249', '-74.044500', '17', 'images/cle.png', '100', 'cle', '');

-- --------------------------------------------------------

--
-- Structure de la table `scores`
--

DROP TABLE IF EXISTS `scores`;
CREATE TABLE IF NOT EXISTS `scores` (
  `pseudo` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `dateheure` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `chrono` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `scores`
--

INSERT INTO `scores` (`pseudo`, `dateheure`, `chrono`) VALUES
('Serge Botton', '06/12/2019 15:13', '00:00:01'),
('Supermaaan', '23/11/2019 09:22', '00:32:37'),
('Jean-Jean', '01/12/2019 03:46', '00:56:29'),
('Mélissa', '02/12/2019 22:49', '00:44:53'),
('Aurore', '03/12/2019 07:31', '00:59:59'),
('Alzo', '30/11/2019 21:35', '05:32:89'),
('Meeez', '07/12/2019 13:17', '03:23:94'),
('Mam', '02/12/2019 23:56', '04:32:21'),
('Chris', '01/12/2019 11:42', '11:32:23'),
('Un acrobate', '01/12/2019 09:44', '00:43:12'),
('Marc', '08/12/2019 22:00', '00:56:73'),
('Meldu', '08/12/2019 21:58', '02:26:54');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
