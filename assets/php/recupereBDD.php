<?php

// Connexion au serveur mySQL
require('connect.php');

// Récupere les icones
$requete = 'SELECT * FROM icones';
$result = $bdd->query($requete);
$requete2 = 'SELECT * FROM enigmes';
$result2 = $bdd->query($requete2);

// Ajoute les données dans un tableau
$reponse = array();
$i = 0;
while ($data = $result->fetch()) {
        $reponse[$i][0] = $data['icone'];
        $reponse[$i][1] = $data['latitude'];
        $reponse[$i][2] = $data['longitude'];
        $reponse[$i][3] = $data['zoom'];
        $reponse[$i][4] = $data['image'];
        $reponse[$i][5] = $data['taille'];
        $reponse[$i][6] = $data['couche'];
        $reponse[$i][7] = $data['nom'];
        $i++;
        // i va de 0 à 6
}

while ($data = $result2->fetch()) {
        $reponse[$i][0] = $data['numero'];
        $reponse[$i][1] = $data['enigme'];
        $reponse[$i][2] = $data['image'];
        $reponse[$i][3] = $data['taille'];
        $reponse[$i][4] = $data['reponse'];
        $reponse[$i][5] = $data['reponse2'];
        $reponse[$i][6] = $data['explication'];
        $i++; // i va de 7 à 12
}

// Envoie des données de type JSON pour les récupérer en javascript
header('Content-Type: application/json');
echo json_encode($reponse);

?>
