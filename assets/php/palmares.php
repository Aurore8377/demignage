<?php

// Connexion au serveur mySQL
require('connect.php');

//Requete SQL : selection des 3 meilleurs joueurs (les 3 plus petits chronos)
$requete2 = 'SELECT * FROM scores ORDER BY chrono LIMIT 3';
$result2 = $bdd->query($requete2);

// Ajoute les 3 meilleurs joueurs dans un tableau
$meilleurs = array();
$i = 0;
while ($data = $result2->fetch()) {
        $meilleurs[$i][0] = $data['pseudo'];
        $meilleurs[$i][1] = $data['dateheure'];
        $meilleurs[$i][2] = $data['chrono'];
        $i++;
}

// Envoie des données de type JSON pour les récupérer en javascript
header('Content-Type: application/json');
echo json_encode($meilleurs);

?>
