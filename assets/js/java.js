﻿/* -------- RECUPERATION DES ICONES ET ENIGMES DE LA BASE DE DONNEES -------- */

Icones = [];
Latitudes = [];
Longitudes = [];
Zooms = [];
ImagesI = [];
TaillesI = [];
Couches = [];
Noms = [];

Numeros = [];
Enigmes = [];
ImagesE = [];
TaillesE = [];
Reponses = [];
Reponses2 = [];
Explications = [];

fetch("assets/php/recupereBDD.php",{
	method: "get",
	headers: {
		'Content-Type': 'application/x-www-form-urlencoded'
	}
})
.then(r => r.json())
.then(r => {
	for (var i = 0; i < 7; i++) {
		Icones.push(r[i][0]);
		Latitudes.push(parseFloat(r[i][1]));
		Longitudes.push(parseFloat(r[i][2]));
		Zooms.push(parseFloat(r[i][3]));
		ImagesI.push(r[i][4]);
		TaillesI.push(parseFloat(r[i][5]));
		Couches.push(r[i][6]);
		Noms.push(r[i][7]);
	}
	for (var i = 7; i < 12; i++) {
		Numeros.push(r[i][0]);
		Enigmes.push(r[i][1]);
		ImagesE.push(r[i][2]);
		TaillesE.push(r[i][3]);
		Reponses.push(r[i][4]);
		Reponses2.push(r[i][5]);
		Explications.push(r[i][6]);
	}

/* ---------------------------- FOND DE CARTE ---------------------------- */

// Création et ajout de la carte (centrée sur la tour Eiffel)
var mymap = L.map('mapid').setView([Latitudes[0],Longitudes[0]], 18);
L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
	minZoom: 1,
	maxZoom: 18,
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

//Boutton pour zoomer et dézoomer:
new L.Control.Zoom({ position: 'bottomright' }).addTo(mymap);

/* ---------------------------- LES ICONES ---------------------------- */


mymap.on('zoomend', function() {
									if (mymap.getZoom() >= 5){ // mode zoomé
											mymap.addLayer(tourEiffel)}
									else{
												mymap.removeLayer(tourEiffel);
											}
										});


// Tour Eiffel
tourEiffel = new L.LayerGroup();//Couches[0]
var tourIcon = L.marker([Latitudes[0], Longitudes[0]], {icon: L.icon({iconUrl: ImagesI[0],
iconSize: [TaillesI[0],TaillesI[0]]})}).addTo(tourEiffel).on('click', onClickTour);//Icones[0]
mymap.addLayer(tourEiffel);


// Statue de la Liberté
statueLiberte = new L.LayerGroup();
var statueIcon = L.marker([Latitudes[1], Longitudes[1]], {icon: L.icon({iconUrl: ImagesI[1],
iconSize: [TaillesI[1],TaillesI[1]]})}).addTo(statueLiberte).on('click', onClickStatue);


// Pyramide de Kheops
pyramide = new L.LayerGroup();
var pyramideIcon = L.marker([Latitudes[2], Longitudes[2]], {icon: L.icon({iconUrl: ImagesI[2],
iconSize: [TaillesI[2],TaillesI[2]]})}).addTo(pyramide).bindPopup("Pyramide bloquée par la clé");


// Opera de Sydney
operaSydney = new L.LayerGroup();
var operaIcon =
L.marker([Latitudes[3], Longitudes[3]], {icon: L.icon({iconUrl: ImagesI[3],
iconSize: [TaillesI[3],TaillesI[3]]})}).addTo(operaSydney).on('click', onClickOpera);


// Christ Rédempteur de Rio
christ = new L.LayerGroup();
var christIcon = L.marker([Latitudes[4], Longitudes[4]], {icon: L.icon({iconUrl: ImagesI[4],
iconSize: [TaillesI[4],TaillesI[4]]})}).addTo(christ).on('click', onClickChrist);


// Cité interdite de Pékin
cite = new L.LayerGroup();
var citeIcon = L.marker([Latitudes[5], Longitudes[5]], {icon:  L.icon({iconUrl: ImagesI[5],
iconSize: [TaillesI[5],TaillesI[5]]})}).addTo(cite).on('click', onClickCite);


// Clé
cle = new L.LayerGroup();
var cleIcon = L.marker([Latitudes[6], Longitudes[6]], {icon: L.icon({iconUrl: ImagesI[6],
iconSize: [TaillesI[6],TaillesI[6]]})}).addTo(cle).on('click', onClickCle);


// Icone avion
var avion = document.createElement("button");
avion.setAttribute("class", "button");
avion.innerHTML = "Accès rapide";
avion.innerHTML = '<img src="images/plane.png" width="60%"height="20%">';

// On cache les couches contenants les markers (sauf la tour Eiffel)
mymap.removeLayer(statueLiberte);
mymap.removeLayer(pyramide);
mymap.removeLayer(operaSydney);
mymap.removeLayer(christ);
mymap.removeLayer(cite);
mymap.removeLayer(cle);


/* ------------------- FONCTION UTILE POUR LA SUITE ------------------------- */

 // Fonction qui retire les accents et les majuscules d'une chaine de caractère
function removeAccentsMaj(str){
      var accents    = 'àáâãäåòóôõöøèéêëðçìíîïùúûüñšÿýž';
      var accentsOut = "aaaaaaooooooeeeeeciiiiuuuunsyyz";
			str = str.toLowerCase();
      str = str.split('');
      var strLen = str.length;
      var i, x;
      for (i = 0; i < strLen; i++) {
        if ((x = accents.indexOf(str[i])) != -1) {
          str[i] = accentsOut[x];
        }
      }
      return str.join('');
 }


/* ---------- FONCTIONS QUI AJOUTENT DES ELEMENTS AUX CONSIGNES ---------- */

// On récupère la div consignes du document html
 var consignes = document.getElementById('consignes');
 var reponses = document.getElementById('reponses');
 reponses.style.visibility='hidden';


// Ajouter du texte aux consignes (str)
function addTexte(str, color, div) {
	var para = document.createElement("p");
	var texte = document.createTextNode(str);
	para.setAttribute("id", color);
	para.appendChild(texte);
	div.appendChild(para);
}


// Ajouter une image de taille height*width aux consignes
function addImage (src, height, width, div) {
	var image = document.createElement('img');
	image.setAttribute("src", src);
	image.setAttribute("height", height);
	image.setAttribute("width", width);
	div.appendChild(image);
}

// Ajouter un bouton aux consignes
function addButton (nom, id, div){
  var btn = document.createElement("button");
  btn.innerHTML = nom;
  btn.setAttribute("id", id);
	btn.setAttribute("class", "button");
  btn.setAttribute("type", "submit");
  div.appendChild(btn);
}

// Fonction qui supprime tous les éléments enfants dans la div consignes
function removeDiv(div){
 if (div.hasChildNodes()) {
  var children = div.childNodes;
  for (var i = 0; i < children.length; i++) {
    div.removeChild(children[i]);
  }
 }
}

// Fonction qui supprime tous les éléments enfants dans la div inventaireBis sauf le titre
function removeInventaireBis(){
 if (inventaireBis.hasChildNodes()) {
  var children = inventaireBis.childNodes;
  for (var i = 1; i < children.length; i++) {
    inventaireBis.removeChild(children[i]);
  }
 }
}



/* ---------- FONCTIONS QUI AJOUTENT DES ELEMENTS A L'INVENTAIRE ---------- */

// On récupère les divs des inventaires du document html
 var inventaire = document.getElementById('inventaire');
 var inventaireBis = document.getElementById('inventaireBis');
 var cleMove = document.getElementById("cleMove");
//On cache pour l'instatn le formulaire pour désamorcer la bombe
 var boutonsCheckbox=document.getElementById("boutonsCheckbox");
 boutonsCheckbox.style.visibility="hidden";


 // Ajouter une image de taille height*width a l'inventaire
 // Et supprime la couche layer de la carte
 function addImageInventaire (src, height, width, layer) {
	removeDiv(consignes); removeDiv(consignes);
 	var image = document.createElement('img');
 	image.setAttribute("src", src);
 	image.setAttribute("height", height);
 	image.setAttribute("width", width);
 	inventaire.appendChild(image);
	layer.clearLayers();
 }


	// Affiche l'inventaire bis contenant la clé
  // Supprime la clé de la carte
  // Et modifie les consignes
   function addInventaireBis () {
	 removeDiv(consignes); removeDiv(consignes);
 	 inventaireBis.style.visibility='visible';
 	 var image = document.createElement('img');
 	 image.setAttribute("src", "images/cle.png");
 	 image.setAttribute("height", "70");
 	 image.setAttribute("width", "70");
 	 inventaireBis.appendChild(image);
 	 image.onclick = function() {onClickCleSelect();};
 	 cle.clearLayers();
 	 // On retire les anciennes consignes
 	 removeDiv(consignes);
 	 // Ajout d'un bouton d'accès rapide au prochain monument
	 avion.onclick = function(){ mymap.flyTo([Latitudes[2],Longitudes[2]], 5); 	removeDiv(consignes); removeDiv(consignes);
 		 addTexte("La pyramide a besoin de la clé pour s'ouvrir, sélectionne la clé de ta poche","noir", consignes);}
 	 consignes.appendChild(avion);
 	 // On ajoute la nouvelle consigne
 	 addTexte("Rend-toi à la pyramide de Khéops, lieu bien connu de dieu Rê/Râ!", "noir", consignes);
   }


 // Masque l'inventaire bis
 function deleteInventaireBis () {
 	inventaireBis.style.visibility='hidden';
 }
 // Suppresion temporaire de l'iventaire qui contiendra la clé
 deleteInventaireBis();


 // Selectionne la clé dans l'inventaire bis
function selectCle () {
 	// Supprime la clé de l'inventaire bis
	removeInventaireBis();

	//On peut déplacer la clé jusqu'à la pyramide
	cleMove.style.visibility='visible';
	document.onmousemove = suitsouris;
	function suitsouris(evenement){
		if(navigator.appName=="Microsoft Internet Explorer"){
				var x = event.x+document.body.scrollLeft;
				var y = event.y+document.body.scrollTop;
		}else{
				var x =  evenement.pageX;
				var y =  evenement.pageY;
		}
		cleMove.style.left = (x+1)+'px';
		cleMove.style.top  = (y+1)+'px';
	}

 	// On retire les anciennes consignes
 	removeDiv(consignes); removeDiv(consignes);
 	// On ajoute la nouvelle consigne
 	addTexte("Tu peux maintenant glisser la clé jusqu'à la pyramide et cliquer dessus pour la visiter", "noir", consignes);
	pyramideIcon.on('click', onClickPyramide);
	//pyramideIcon.removeBindPopup();// TO DO
  }


/* --------------- FONCTIONS QUI MODIFIENT LES CONSIGNES --------------- */


// Fonction qui supprime les anciennes consignes et qui les
// remplace par les indices de l'enigme
function addEnigme (enigme, image){
		// On retire les anciennes consignes
		removeDiv(consignes);
		removeDiv(consignes);
		removeDiv(consignes);
		// On ajoute l'enigme et le champs de réponse
		addTexte(enigme, "noir", consignes);
		addImage(image, "100%", "100%", consignes);
		consignes.appendChild(document.createElement("br"));

}


// Fonction qui ajoute une zone de texte input et un bouton submit
// Si la réoinse est bonne, le prochain monument apparait en appuyant sur Continuer
function addInput (reponse1, reponse2, explications, icone, layer, monument) {
	// Crétion d'une zone de texte input
	var input = document.createElement("input");
	input.setAttribute("type", 'text');
	input.setAttribute("id", 'input');
	input.setAttribute("placeholder", 'Ta réponse');
  consignes.appendChild(input);

	input.addEventListener("blur", fct = function (e) {
			// Si la réponse saisie est bonne
			if ((removeAccentsMaj(e.target.value).trim() === (reponse1.trim())) || (removeAccentsMaj(e.target.value).trim() === (reponse2.trim()))){
				removeDiv(reponses);
				validite="Bonne réponse";
				reponses.style.visibility = 'visible';
				addTexte(validite + " : " + e.target.value, "vert", reponses);
				addTexte(explications, "noir", reponses);
				addButton("Continuer", "continuer", reponses);
				continuer = document.getElementById("continuer");
				continuer.onclick = function() {
					  reponses.style.visibility = 'hidden';
						removeDiv(reponses);removeDiv(reponses);removeDiv(reponses);
						bonneReponse(icone, layer, monument);
				}
				input.removeEventListener("blur", fct);
			}
			else{ // Si la réponse saisie est mauvaise
				removeDiv(reponses);
				validite="Mauvaise Réponse !";
				reponses.style.visibility = 'visible';
				addTexte(validite, "rouge", reponses);
				}
			});
}


// Fonction qui ajoute un icone sur la carte et qui modifie les consignes
function bonneReponse(icone, layer, monument){
	// On retire les anciennes consignes
	removeDiv(consignes);
	removeDiv(consignes);
	removeDiv(consignes);
	removeDiv(consignes);
	// On ajoute l'icone sur l'écran en fonction du zoom
	mymap.on('zoomend', function() {
							if (mymap.getZoom() >= 5){ // mode zoomé
									mymap.addLayer(layer)}
							else{
								mymap.removeLayer(layer);
							}
								});
	// Ajout d'un bouton d'accès rapide au prochain monument
	avion.onclick = function(){ mymap.flyTo([icone.getLatLng().lat, icone.getLatLng().lng], 5); removeDiv(consignes);}
	consignes.appendChild(avion);
	// On ajoute la nouvelle consigne (cliquer sur le prochain monument)
	addTexte("Pour visiter " + monument + ", clique dessus", "noir", consignes); // TO DO : mission 1, mission 2...
}

// Fonction qui écrit les consignes en cas de bombe absente
function bombeAbsente(monument) {
	removeDiv(consignes);
	addTexte("La bombe n'est pas à " + monument + " !", "noir", consignes);
	consignes.appendChild(document.createElement("br"));
	addImage ("images/bombeNON.png", "70", "70", consignes);
}


// Fonction qui ajoute une zone de texte input et un bouton submit
// Si la réoinse est bonne, la clé apparait en appuyant sur afficher la clé
function addInputCle (reponse1, reponse2, explications, layer) {
	// Crétion d'une zone de texte input
	var input = document.createElement("input");
	input.setAttribute("type", 'text');
	input.setAttribute("id", 'input');
	input.setAttribute("placeholder", 'Ta réponse');
  consignes.appendChild(input);

	input.addEventListener("blur", fct = function (e) {
			// Si la réponse saisie est bonne
			if ((removeAccentsMaj(e.target.value).trim() === (reponse1.trim())) || (removeAccentsMaj(e.target.value).trim() === (reponse2.trim()))){
				removeDiv(reponses);
				validite="Bonne réponse";
				reponses.style.visibility = 'visible';
				addTexte(validite + " : " + e.target.value, "vert", reponses);
				addTexte(explications, "noir", reponses);
				addButton("Afficher la clé", "continuerCle", reponses);
				continuer = document.getElementById("continuerCle");
				continuer.onclick = function() {
					  reponses.style.visibility = 'hidden';
					  removeDiv(reponses);removeDiv(reponses);removeDiv(reponses);
						bonneReponseCle(layer);
				}
				input.removeEventListener("blur", fct);
				// Quand la réponse est bonne, la pyramide (bloquée par la clé) apparaît
				mymap.on('zoomend', function() {
										if (mymap.getZoom() >= 5){ // mode zoomé
												mymap.addLayer(pyramide)}
												else{
													mymap.removeLayer(pyramide);
												}
											});
			}
			else{ // Si la réponse saisie est mauvaise
				removeDiv(reponses);
				validite="Mauvaise Réponse !";
				reponses.style.visibility = 'visible';
				addTexte(validite, "rouge", reponses);
				}
			});
}


// Fonction qui ajoute la clé sur la carte et qui modifie les consignes
function bonneReponseCle(layer){
	// On retire les anciennes consignes
	removeDiv(consignes);
	removeDiv(consignes);
	removeDiv(consignes);
	removeDiv(consignes);
	// On ajoute l'icone sur l'écran
	mymap.addLayer(layer);
	// On ajoute la nouvelle consigne (cliquer sur le prochain monument)
	addTexte("Pour mettre la clé dans ta poche, clique dessus", "noir", consignes);
}


// Fonction qui écrit les consignes en cas de bombe présente
function bombePresente() {
	// Modification des consignes
	removeDiv(consignes);
	addTexte("La bombe est ici !", "noir", consignes);
	consignes.appendChild(document.createElement("br"));
	addImage ("images/bombe.png", "70", "70", consignes);
	// Bouton "désamorcer la bombe" qui permet d'arrêter le chronomètre
	// On récupère ce chrono ainsi que la date du jour
	document.getElementById("boutonsCheckbox").style.visibility="visible";
	document.getElementById("choix1").onclick = function(){document.location.href="gameOver.html"; }
	document.getElementById("choix2").onclick = function(){document.location.href="gameOver.html";}
	document.getElementById("choix3").onclick = function(){
	addButton ("Désamorcer la bombe", "fin", consignes)
	fin = document.getElementById("fin");
	fin.onclick = function() {
		addTexte("Bravo, tu as réussi à désamorcer la bombe à temps !", "noir", consignes);
		// Stoppe le chrono et récupère la date
		var chrono = document.getElementById("chronotime").innerHTML;
		var date = new Date();
		var dateheure = formatage(date.getDate())+"/"+formatage(date.getMonth()+1)+"/"+date.getFullYear()+" "+formatage(date.getHours())+":"+formatage(date.getMinutes());
		document.getElementById("chronotime").style.visibility='hidden';
		addTexte("Tu as mis " + chrono + " minutes pour désamorcer la bombe !", "rouge", consignes);
		addButton ("Continuer", "next", consignes)
		next = document.getElementById("next");
		next.onclick = function() {
			location.href="formulaire.html?dateheure='" + dateheure + "'&chrono='" + chrono + "'";}
	}
}
}


/* -------------------------- DEMARRAGE DU JEU -------------------------- */


/* 1) Initialisation : clic sur la tour Eiffel */

// Consigne
addTexte("Clique sur la tour Eiffel pour la visiter", "noir", consignes);

// Enchainement d'actions lors d'un clic sur la tour Eiffel
function onClickTour() {
	// Ajout de l'icone à l'invenraire
													addImageInventaire('images/tourEiffelIcon.png', "70", "70", tourEiffel);
						              // Modifier les consignes
													bombeAbsente(Noms[0]);
						              // Ajout d'un bouton qui affiche l'enigme ainsi qu'un champs de réponse
						              addButton ("Voir l'enigme qui mène au prochain monument", Numeros[0], consignes);
						              bouton1 = document.getElementById(Numeros[0]);
						              bouton1.onclick = function() {
						                  addEnigme (Enigmes[0], ImagesE[0]);
						                  addInput(Reponses[0], Reponses2[0], Explications[0], statueIcon, statueLiberte, Noms[1]);
															addButton("Valider", "", consignes);
						              }
										}

/* 2) Clic sur la statue de la Liberté */

// Enchainement d'actions lors d'un clic sur la statue de la Liberté
function onClickStatue() { 	// Ajout de l'icone à l'invenraire
														addImageInventaire('images/statueLiberteIcon.png', "70", "70", statueLiberte);
							              // Modifier les consignes
														bombeAbsente(Noms[1]);
							              // Ajout d'un bouton qui affiche l'enigme ainsi qu'un champs de réponse
							              addButton ("Voir l'enigme qui mène au prochain monument", Numeros[1], consignes);
							              bouton2 = document.getElementById(Numeros[1]);
							              bouton2.onclick = function() {
							                  addEnigme (Enigmes[1], ImagesE[1]);
							                  addInputCle(Reponses[1], Reponses2[1], Explications[1], cle);
																addButton("Valider", "", consignes);
																}
						              	}

/* 3) Clic sur la clé */


function onClickCle() {// Affichage de l'iventaire qui contiendra la clé et des nouvelles consignes
											 addInventaireBis ();
}

function onClickCleSelect() {// Selection de la clé en cliquant dessus
															selectCle ();
}


/* 4) Clic sur la Pyramide de Khéops */

// Enchainement d'actions lors d'un clic sur la pyramide de Khéops
function onClickPyramide() {//La clef disparaît quand on clique sur la pyramide:
														cleMove.style.visibility='hidden';
														// Stopper le onmousemove en mettant le curseur à (0,0)
														document.onmousemove = stopsouris;
														function stopsouris(evenement){
																	var x = 0;
																	var y = 0;
															cleMove.style.left = (x)+'px';
															cleMove.style.top  = (y)+'px';
														}
														// Ajout de l'icone à l'invenraire
														addImageInventaire('images/pyramideKheopsIcon.png', "70", "70", pyramide);
														// Masquer l'inventaire qui contenait la clé
														deleteInventaireBis ();
							              // Modifier les consignes
														bombeAbsente(Noms[2]);
							              // Ajout d'un bouton qui affiche l'enigme ainsi qu'un champs de réponse
							              addButton ("Voir l'enigme qui mène au prochain monument", Numeros[2], consignes);
							              bouton2 = document.getElementById(Numeros[2]);
							              bouton2.onclick = function() {
							                  addEnigme (Enigmes[2], ImagesE[2]);
							                  addInput(Reponses[2], Reponses2[2], Explications[2],operaIcon, operaSydney, Noms[3]);
																addButton("Valider", "", consignes);
						              	}
}

/* 5) Clic sur l'Opéra de Sydney */

// Enchainement d'actions lors d'un clic sur l'Opéra de Sydney
function onClickOpera() {// Ajout de l'icone à l'invenraire
												addImageInventaire('images/operaSydneyIcon.png', "70", "70", operaSydney);
					              // Modifier les consignes
												bombeAbsente(Noms[3]);
					              // Ajout d'un bouton qui affiche l'enigme ainsi qu'un champs de réponse
					              addButton ("Voir l'enigme qui mène au prochain monument", Numeros[3], consignes);
					              bouton2 = document.getElementById(Numeros[3]);
					              bouton2.onclick = function() {
					                  addEnigme (Enigmes[3], ImagesE[3]);
					                  addInput(Reponses[3], Reponses2[3], Explications[3], christIcon, christ, Noms[4]);
														addButton("Valider", "", consignes);
				              	}
}


/* 6) Clic sur le Christ Rédempteur */

// Enchainement d'actions lors d'un clic sur le Christ Rédempteur
function onClickChrist() {// Ajout de l'icone à l'invenraire
													addImageInventaire('images/christRioIcon.png', "70", "70", christ);
						              // Modifier les consignes
													bombeAbsente(Noms[4]);
						              // Ajout d'un bouton qui affiche l'enigme ainsi qu'un champs de réponse
						              addButton ("Voir l'enigme qui mène au prochain monument", Numeros[4], consignes);
						              bouton2 = document.getElementById(Numeros[4]);
						              bouton2.onclick = function() {
						                  addEnigme (Enigmes[4], ImagesE[4]);
						                  addInput(Reponses[4], Reponses2[4], Explications[4], citeIcon, cite, Noms[5]);
															addButton("Valider", "", consignes);
					              	}
}


/* 7) Clic sur la Cité Interdite */

// Enchainement d'actions lors d'un clic sur la Cité Interdite
function onClickCite() {// Ajout de l'icone à l'invenraire
												addImageInventaire('images/citeInterditeIcon.png', "70", "70", cite);
						            // Modifier les consignes
												bombePresente();


}

});// Fin du fetch

// Fonction qui ajoute un 0 devant les nombre inférieurs à 10
function formatage(nombre){
	if (nombre<10){
		return "0"+nombre;
	} else {
		return nombre;
	}
}


var timerID = 0;
var min=00;
var sec=00;
var msec=00;

function updateChrono(){
if(min<30){ //Chrono qui dure 15min
	if(msec<99){
		msec=msec+1;
	}
	else if(sec<59){
		msec=0
		sec=sec+1
	}
	else{
		sec=0
		msec=0
		min = min+1
	}
	document.getElementById("chronotime").innerHTML =  formatage(min) + ":" + formatage(sec)+ ":"+ formatage(msec);
	timerID = setTimeout("updateChrono()", 10); // Réactualisation toutes les secondes
} else{
	document.location.href="gameOver.html";

 }
}

updateChrono()
