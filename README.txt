# DemIGNage

Il s�agit d�un G�oEscapeGame

## Auteurs

Aurore Alarcon et M�lissa Dupuis

# Pr�sentation 

Les donn�es se r�partissent de la fa�on suivante :

- Les fichiers html et les m�dias (images + sons) � la racine
- Un dossier assets contenant les fichiers js, les php et les css
- Une base de donn�es projet.sql contenant les tables n�cessaires pour importer les m�dias, les icones, les enigmes et ins�rer les scores

## Installation

Installez sur votre ordinateur un simulateur de serveur WEB, par exemple :
MAMP sur https://www.mamp.info/en/downloads/
WAMP sur https://sourceforge.net/projects/wampserver/

T�l�chargez Atom ou un autre �diteur si vous souhaitez consulter et modifier les codes (html/css/js/php)
https://atom.io/

Ensuite, via votre serveur allez sur phpMyAdmin et importez la base de donn�es projet.sql fournie dans les donn�es

Dans le fichier assets/php/connect.php, changez le chemin d�acc�s aux donn�es pour l�adapter � votre serveur.
Par exemple sur MAMP, le couple utilisateur/mot de passe est 'root'/'root' alors que sous WAMP c'est 'root'/''

## Utilisation

Lancez votre simulateur de serveur, puis entrez dans votre navigateur pr�f�r� l'url 'localhost'
V�rifiez que la premi�re page html sur laquelle vous arrivez correspond bien � index.html
Au lancement vous aurez �galement besoin d��tre connect� � internet pour que le fond de carte se charge correctement

Vous pouvez � pr�sent jouer si vous acceptez votre mission. Bonne chance !

## Contribution

Toute proposition de modification est la bienvenue ; notamment pour ajouter un clavier pour composer le code pour la bombe ou pour am�liorer le CSS
